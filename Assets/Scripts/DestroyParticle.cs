﻿using UnityEngine;
using System.Collections;

public class DestroyParticle : MonoBehaviour 
{
    void Start()
    {
        Destroy(gameObject, 5f);
    }
}
