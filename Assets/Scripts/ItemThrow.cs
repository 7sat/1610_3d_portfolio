﻿using UnityEngine;
using System.Collections;

public class ItemThrow : MonoBehaviour 
{
    ItemInfo iteminfo;

    public bool canBeThrown;
    public float throwForce;

    void Awake()
    {
        iteminfo = GetComponent<ItemInfo>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F) && Time.timeScale > 0 && canBeThrown && transform.root.CompareTag("Player"))
        {
            ThrowActions();
        }
    }

    public void ThrowActions()
    {
        if(GetComponent<ItemInfo>().itemType == ItemInfo.ItemType.Gun)
        {
            GetComponent<GunAmmo>().ResetGunReloading();
            GetComponent<HighLightToggle>().HighlightOn();

            foreach (Transform tf in gameObject.transform)
            {
                tf.gameObject.layer = LayerMask.NameToLayer("Item");
            }

            if(PlayerInventory.Instance.currentlyHeldItem != null)
            {
                if (PlayerInventory.Instance.currentlyHeldItem.gameObject == gameObject)
                {
                    PlayerAmmoBox.Instance.HideAmmoUI();
                    GunAccuIndicator.Instance.HideIndicator();
                    PlayerInventory.Instance.currentlyHeldItem = null;
                }
            }
        }

        Vector3 throwDir = transform.parent.forward;

        GetComponent<BoxCollider>().isTrigger = false;
        GetComponent<Rigidbody>().isKinematic = false;
        GetComponent<Animator>().enabled = false;
        transform.parent = null;

        GetComponent<Rigidbody>().AddForce(throwDir * throwForce, ForceMode.Impulse);

        for(int i = 0; i<18; i++)
        {
            if(PlayerInventory.Instance.InvenItemTF[i] == gameObject.transform)
            {
                PlayerInventory.Instance.InvenItemTF[i] = null;
                break;
            }
        }
        PlayerInventory.Instance.UpdateInvenListAndUI();
    }
}
