﻿using UnityEngine;
using System.Collections;

public class MouseLock : MonoBehaviour 
{
    public bool isCursorLock;

    void Start()
    {
        ToggleCursorLock();
    }

    void Update () 
	{
	    if(Input.GetKeyDown(KeyCode.P))
        {
            ToggleCursorLock();
        }
	}

    public void ToggleCursorLock()
    {
        if (isCursorLock)
        {
            Cursor.lockState = CursorLockMode.None;
        }
        else
        {
            Cursor.lockState = CursorLockMode.Locked;
        }
        Cursor.visible = !Cursor.visible;
        isCursorLock = !isCursorLock;
    }
}
