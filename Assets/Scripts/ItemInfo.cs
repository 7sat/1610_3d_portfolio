﻿using UnityEngine;
using System.Collections;

public class ItemInfo : MonoBehaviour 
{
    public string itemName;
    public Vector3 itemRotation;
    public enum ItemType { Gun, Etc }
    public ItemType itemType = ItemType.Etc;

    public int itemCode;

    void Awake()
    {
        gameObject.name = itemName;
    }
}
