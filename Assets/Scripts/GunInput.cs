﻿using UnityEngine;
using System.Collections;

public class GunInput : MonoBehaviour
{
    private float nextAttack;
    public float attackRate;
    public bool isAutomatic;

    bool isBurstFireActive;
    public bool isGunLoaded;
    public bool isReloading;

    GunShoot gunShoot;
    GunAmmo gunAmmo;
    Animator weaponPosAnim;

    void Start()
    {
        gunShoot = GetComponent<GunShoot>();
        gunAmmo = GetComponent<GunAmmo>();
        isGunLoaded = true;
        weaponPosAnim = Camera.main.transform.GetChild(0).GetComponent<Animator>();
    }

    void Update()
    {
        CheckIfWeaponShouldAttack();
        CheckAimed();
        BurstFireToggle();
        Reload();
    }

    void CheckIfWeaponShouldAttack()
    {
        if (Time.time > nextAttack && Time.timeScale > 0 && gameObject.transform.root.CompareTag("Player"))
        {
            if (isAutomatic && !isBurstFireActive)
            {
                if (Input.GetMouseButton(0))
                {
                    AttemptAttack();
                }
            }
            else if (isAutomatic && isBurstFireActive) // 총이 오토매틱이고 연발모드임.
            {
                if (Input.GetMouseButtonDown(0))
                {
                    StartCoroutine(BurstFireCor());
                }
            }
            else if (!isAutomatic) //단발.
            {
                if (Input.GetMouseButtonDown(0))
                {
                    AttemptAttack();
                }
            }
        }
    }

    void CheckAimed()
    {
        if(Time.timeScale > 0 && gameObject.transform.root.CompareTag("Player"))
        {
            if (Input.GetMouseButton(1))
            {
                weaponPosAnim.SetBool("Aim", true);
                PlayerState.Instance.isAim = true;
            }
            else
            {
                weaponPosAnim.SetBool("Aim", false);
                PlayerState.Instance.isAim = false;
            }
        }
    }

    void AttemptAttack()
    {
        nextAttack = Time.time + attackRate;
        if (isGunLoaded)
        {
            gunShoot.OpenFire();
            gunAmmo.DeductAmmo();
        }
    }

    void Reload()
    {
        if (Input.GetKeyDown(KeyCode.R) && Time.timeScale > 0 && gameObject.transform.root.CompareTag("Player"))
        {
            gunAmmo.TryToReload();
        }
    }

    void BurstFireToggle()
    {
        if (Input.GetKeyDown(KeyCode.T) && Time.timeScale > 0 && gameObject.transform.root.CompareTag("Player"))
        {
            isBurstFireActive = !isBurstFireActive;
        }
    }

    IEnumerator BurstFireCor()
    {
        AttemptAttack();
        yield return new WaitForSeconds(attackRate);
        AttemptAttack();
        yield return new WaitForSeconds(attackRate);
        AttemptAttack();
    }
}


