﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PlayerAmmoBox : MonoBehaviour 
{
    static PlayerAmmoBox instance;
    public static PlayerAmmoBox Instance { get { return instance; } }

    void Awake()
    {
        instance = this;
    }

    [System.Serializable]
    public class AmmoTypes
    {
        public string ammoName;
        public int ammoMaxQuantity;
        public int ammoCurrentCarried;

        public AmmoTypes(string aName, int aMaxQuantity, int aCurrentCarried)
        {
            ammoName = aName;
            ammoMaxQuantity = aMaxQuantity;
            ammoCurrentCarried = aCurrentCarried;
        }
    }


    public List<AmmoTypes> typesOfAmmunition = new List<AmmoTypes>();

    void OnTriggerEnter(Collider col)
    {
        if(col.tag == "AmmoBox")
        {
            for(int i = 0; i< col.GetComponent<AmmoBox>().ammoType.Length; i++)
            {
                PickedUpAmmo(col.GetComponent<AmmoBox>().ammoType[i], col.GetComponent<AmmoBox>().quantity[i]);
            }
        }
    }

    void PickedUpAmmo(string ammoName, int quantity) // 무슨탄을 얼마만큼.
    {
        for (int i = 0; i < typesOfAmmunition.Count; i++)
        {
            if (typesOfAmmunition[i].ammoName == ammoName) // 일치하는 탄종을 찾음.
            {
                typesOfAmmunition[i].ammoCurrentCarried += quantity;

                if (typesOfAmmunition[i].ammoCurrentCarried > typesOfAmmunition[i].ammoMaxQuantity) // 최대 보유량 이상이면 깍음.
                {
                    typesOfAmmunition[i].ammoCurrentCarried = typesOfAmmunition[i].ammoMaxQuantity;
                }

                UpdateAmmoUI();

                break;
            }
        }
    }

    public GameObject ammoPanel;
    public Text weaponName;
    public Text ammoText;
    public GunAmmo gunAmmo;

    public void UpdateAmmoUI()
    {
        if (gunAmmo == null) return;

        for (int i = 0; i < typesOfAmmunition.Count; i++)
        {
            if (typesOfAmmunition[i].ammoName == gunAmmo.ammoName) // 일치하는 탄종을 찾음.
            {
                ammoText.text = gunAmmo.currentAmmo + " / " + typesOfAmmunition[i].ammoCurrentCarried;
                break;
            }
        }
    }

    public void ShowAmmoUI()
    {
        weaponName.text = gunAmmo.gameObject.name;
        ammoPanel.SetActive(true);
    }
    public void HideAmmoUI()
    {
        ammoPanel.SetActive(false);
    }
}
