﻿using UnityEngine;
using System.Collections;

public class weaponHighLight : MonoBehaviour 
{
    public Renderer rend;
    public Material[] defaultMat;
    public Material[] highlightMat;

    public bool isHighLighted;

	void Awake()
    {
        rend = GetComponent<Renderer>();
    }

    public void HighLightOn()
    {
        rend.sharedMaterials = highlightMat;
        isHighLighted = true;
    }

    public void HighLightOff()
    {
        rend.sharedMaterials = defaultMat;
        isHighLighted = false;
    }
}
