﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GunAccuIndicator : MonoBehaviour 
{
    static GunAccuIndicator instance;
    public static GunAccuIndicator Instance { get { return instance; } }

    public Animator animator;
    public GameObject indicator;

    public GunShoot gunShoot;

    void Awake()
    {
        instance = this;
    }

	void Update () 
	{
        if(indicator.activeSelf)
        {
            float value = PlayerState.Instance.playerCurrentSpeed / 20 + (gunShoot.AccuracyLowerByShooting * 10);
            if (PlayerState.Instance.isAim == false) value += 0.15f;
            if (value > 1) value = 1;
            animator.SetFloat("PlayerMoveSpd", value);
        }
	}

    public void ShowIndicator()
    {
        indicator.SetActive(true);
    }

    public void HideIndicator()
    {
        indicator.SetActive(false);
    }
}
