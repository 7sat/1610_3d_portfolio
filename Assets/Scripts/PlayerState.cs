﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;

public class PlayerState : MonoBehaviour 
{
    static PlayerState instance;
    public static PlayerState Instance { get { return instance; } }

    FirstPersonController fpsCtr;
    public float playerCurrentSpeed;

    public bool isAim;

    void Awake()
    {
        instance = this;
        fpsCtr = GetComponent<FirstPersonController>();
    }

    void Update()
    {
        playerCurrentSpeed = (fpsCtr.m_MoveDir + new Vector3 (0,fpsCtr.m_StickToGroundForce,0)).magnitude;
        fpsCtr.isAim = isAim;
    }
}
