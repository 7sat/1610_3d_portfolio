﻿using UnityEngine;
using System.Collections;

public class SpawnMob : MonoBehaviour 
{
    public GameObject mobPF;
    public Transform[] pos;
    bool Used;

    void OnTriggerEnter(Collider col)
    {
        if(col.tag == "Player" && !Used)
        {
            Used = true;
            Spawn();
        }
    }

	public void Spawn() 
	{
        for(int i = 0; i<pos.Length; i++)
        {
            Instantiate(mobPF, pos[i].position, pos[i].rotation);
        }
    }
}
