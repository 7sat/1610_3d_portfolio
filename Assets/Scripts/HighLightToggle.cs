﻿using UnityEngine;
using System.Collections;

public class HighLightToggle : MonoBehaviour 
{
	public void HighlightOn() 
	{
	    foreach(Transform tf in gameObject.transform)
        {
            if(tf.tag == "Item")
            {
                tf.GetComponent<weaponHighLight>().HighLightOn();
            }
        }
	}

    public void HighlightOff()
    {
        foreach (Transform tf in gameObject.transform)
        {
            if (tf.tag == "Item")
            {
                tf.GetComponent<weaponHighLight>().HighLightOff();
            }
        }
    }

    void Update()
    {
        if (transform.root.tag == "Player") return;

        if (Vector3.Distance(Camera.main.transform.position, transform.position) > 12)
        {
            HighlightOff();
        }
        else
        {
            HighlightOn();
        }
    }
}
