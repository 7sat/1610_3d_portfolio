﻿using UnityEngine;
using System.Collections;

public class ItemPickUp : MonoBehaviour 
{
    public LayerMask layerToDetect;
    public Transform rayTransformPivot;

    Transform itemAvailableForPickup;
    RaycastHit hit;
    float detectRange = 3;
    float detectRadius = 0.7f;
    bool itemInRange;

    public Transform weaponPos;

    float labelWidth = 150;
    float labelHeight = 40;

    void Update()
    {
        if (Physics.SphereCast(rayTransformPivot.position, detectRadius, rayTransformPivot.forward, out hit, detectRange, layerToDetect))
        {
            itemAvailableForPickup = hit.transform;
            itemInRange = true;
        }
        else
        {
            itemInRange = false;
        }

        if (Input.GetKeyDown(KeyCode.E) && Time.timeScale > 0 && itemInRange && itemAvailableForPickup.root.tag != "Player" && itemAvailableForPickup.tag == "Item")
        {
            PickUp();
        }
    }

    void PickUp()
    {
        int availableSlotNum = 16;

        for (int i = 16; i<18; i++)
        {
            if (PlayerInventory.Instance.InvenItemTF[i] == null)
            {
                break;
            }
            availableSlotNum++;
        }
        if (availableSlotNum == 18 && PlayerInventory.Instance.InvenItemTF[17] != null)
        {
            availableSlotNum = 0;

            for (int i = 0; i < 16; i++)
            {
                if (PlayerInventory.Instance.InvenItemTF[i] == null)
                {
                    break;
                }
                availableSlotNum++;
            }

            if (availableSlotNum == 16 && PlayerInventory.Instance.InvenItemTF[15] != null)
            {
                print("인벤 빈공간 없음.");
                return;
            }
        }

        if (itemAvailableForPickup.GetComponent<ItemInfo>().itemType == ItemInfo.ItemType.Gun)
        {
            itemAvailableForPickup.GetComponent<HighLightToggle>().HighlightOff();

            foreach (Transform tf in itemAvailableForPickup)
            {
                tf.gameObject.layer = LayerMask.NameToLayer("Weapon");
            }

            PlayerAmmoBox.Instance.gunAmmo = itemAvailableForPickup.GetComponent<GunAmmo>();
            GunAccuIndicator.Instance.gunShoot = itemAvailableForPickup.GetComponent<GunShoot>();

            PlayerAmmoBox.Instance.UpdateAmmoUI();
            PlayerAmmoBox.Instance.ShowAmmoUI();
            GunAccuIndicator.Instance.ShowIndicator();
        }

        itemAvailableForPickup.GetComponent<BoxCollider>().isTrigger = true;
        itemAvailableForPickup.GetComponent<Rigidbody>().isKinematic = true;
        itemAvailableForPickup.GetComponent<Animator>().enabled = true;

        itemAvailableForPickup.transform.position = Vector3.zero;
        itemAvailableForPickup.SetParent(weaponPos, false);
        itemAvailableForPickup.transform.localEulerAngles = itemAvailableForPickup.GetComponent<ItemInfo>().itemRotation;

        PlayerInventory.Instance.currentlyHeldItem = itemAvailableForPickup;
        PlayerInventory.Instance.UpdateInvenListAndUI();
        PlayerInventory.Instance.InvenItemTF[availableSlotNum] = itemAvailableForPickup;
        PlayerInventory.Instance.ActivateInvenItem(availableSlotNum);

        Transform oldWeapon = PlayerInventory.Instance.InvenItemTF[16];
        PlayerInventory.Instance.InvenItemTF[16] = PlayerInventory.Instance.InvenItemTF[availableSlotNum];
        PlayerInventory.Instance.InvenItemTF[availableSlotNum] = oldWeapon;
    }

    public GUIStyle guiStyle;

    void OnGUI()
    {
        if (itemInRange && itemAvailableForPickup != null)
        {
            if(itemAvailableForPickup.tag == "Item")
            {
                GUI.Box(new Rect(Screen.width / 2 - labelWidth / 2, Screen.height / 2, labelWidth, labelHeight), itemAvailableForPickup.name + "\n 줍기 : E", guiStyle);
            }
        }
    }
}
