﻿using UnityEngine;
using System.Collections;

public class MonsterMove : MonoBehaviour 
{
    public enum STATE { IDLE, TRACE, ATTACK, DIE } // 아이들시 대기,걷기 반복함.
    public STATE state = STATE.IDLE;

    GameObject playerObj;
    public LayerMask wallMask;
    public float SearchDistance;
    public float AttackDistance;

    public float WalkSpeed;
    public float RunSpeed;
    public bool isStun;
    public float Damage;

    public GameObject mesh;

    Animator anim;
    NavMeshAgent navMeshAgent;
    Vector3 playerLastPos;

    


    void Awake()
    {
        playerObj = GameObject.FindGameObjectWithTag("Player");
        anim = GetComponent<Animator>();
        navMeshAgent = GetComponent<NavMeshAgent>();
    }

	void Start () 
	{
        StartCoroutine(SearchPlayerCor());
        StartCoroutine(IdleCor());
    }

    public void ShowMesh()
    {
        mesh.SetActive(true);
    }

    void SearchPlayer() // IDLE,ATTACK,TRACE 중 한가지로 설정됨.
    {
        float dist = Vector3.Distance(transform.position, playerObj.transform.position);
        Vector3 dir = playerObj.transform.position - transform.position;

        if(PlayerHp.Instance.isGameOver)
        {
            state = STATE.IDLE;
            anim.SetBool("Attack", false);
            return;
        }
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Spawn") || isStun) return;

        if (dist > SearchDistance) // 플레이어가 너무 멀리 있음.
        {
            if(state == STATE.TRACE && Vector3.Distance(playerLastPos,transform.position)>2)
            {
                navMeshAgent.SetDestination(playerLastPos);
                Debug.DrawLine(transform.position, playerLastPos, Color.yellow, 0.5f);
                return;
            }

            if(state != STATE.IDLE)
            {
                state = STATE.IDLE;
                anim.SetBool("Run", false);
                anim.SetBool("Attack", false);
                StartCoroutine(IdleCor());
                navMeshAgent.speed = WalkSpeed;
            }
        }
        else
        {
            if (Physics.Raycast(transform.position + new Vector3(0,2,0), dir, dist, wallMask)) // 장애물 때문에 플레이어를 볼 수 없음.
            {
                if (state == STATE.TRACE && Vector3.Distance(playerLastPos, transform.position) > 2)
                {
                    navMeshAgent.SetDestination(playerLastPos);
                    Debug.DrawLine(transform.position, playerLastPos, Color.yellow, 0.5f);
                    return;
                }

                if (state != STATE.IDLE)
                {
                    state = STATE.IDLE;
                    anim.SetBool("Run", false);
                    anim.SetBool("Attack", false);
                    StartCoroutine(IdleCor());
                    navMeshAgent.speed = WalkSpeed;
                }
            }
            else // 적이 플레이어를 발견함.
            {
                if (dist <= AttackDistance) // 공격거리 안쪽임.
                {
                    state = STATE.ATTACK;
                    anim.SetBool("Attack", true);
                    anim.SetBool("Run", false);
                    anim.SetBool("Walk", false);
                    navMeshAgent.Stop();
                }
                else
                {
                    Debug.DrawLine(transform.position, playerLastPos, Color.red, 0.5f);
                    state = STATE.TRACE;
                    anim.SetBool("Attack", false);
                    anim.SetBool("Run", true);
                    anim.SetBool("Walk", true);
                    navMeshAgent.Resume();
                    navMeshAgent.SetDestination(playerObj.transform.position);
                    navMeshAgent.speed = RunSpeed;
                    playerLastPos = playerObj.transform.position;
                }
            }
        }
    }

    IEnumerator SearchPlayerCor()
    {
        while(state != STATE.DIE)
        {
            SearchPlayer();
            yield return new WaitForSeconds(Random.Range(0.3f,0.6f));
        }
    } // SearchPlayer를 1초에 한번씩 실행함.

    IEnumerator IdleCor()
    {
        while (state == STATE.IDLE)
        {
            if (!anim.GetCurrentAnimatorStateInfo(0).IsName("Spawn") || isStun)
            {
                Vector3 targetPos = Vector3.zero;

                targetPos = transform.position + new Vector3(Random.Range(-5, 5), 0, Random.Range(-5, 5));
                targetPos.y = Terrain.activeTerrain.SampleHeight(targetPos);

                NavMeshPath path = new NavMeshPath();
                navMeshAgent.CalculatePath(targetPos, path);

                if (path.status == NavMeshPathStatus.PathComplete)
                {
                    navMeshAgent.Resume();
                    navMeshAgent.SetDestination(targetPos);
                    anim.SetBool("Walk", true);

                    //Debug.DrawLine(transform.position, navMeshAgent.destination, Color.green, 2f);
                    yield return new WaitForSeconds(0.2f);
                    yield return new WaitUntil(() => navMeshAgent.remainingDistance <= 1);

                    navMeshAgent.Stop();
                    anim.SetBool("Walk", false);

                    yield return new WaitForSeconds(Random.Range(2f,4f));
                }
            }
            yield return null;
        }
    }

    public void SendDmgToPlayer()
    {
        playerObj.SendMessage("PlayerGetDamage", Damage);
    }
}
