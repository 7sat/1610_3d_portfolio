﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.ImageEffects;

public class FinalSpawn : MonoBehaviour 
{
    static FinalSpawn instance;
    public static FinalSpawn Instance { get { return instance; } }

    void Awake()
    {
        instance = this;
    }

    public GameObject mobPF;
    public Transform[] pos;
    bool Used;
    public bool Win;
    public GameObject victoryPanel;
    public GameObject dotPanel;

    List<GameObject> mob = new List<GameObject>();

    void OnTriggerEnter(Collider col)
    {
        if(col.tag == "Player" && !Used)
        {
            Used = true;
            Spawn();
        }
    }

	public void Spawn() 
	{
        if(GetComponent<AudioSource>() != null) GetComponent<AudioSource>().Play();
        for(int i = 0; i<pos.Length; i++)
        {
            GameObject go = Instantiate(mobPF, pos[i].position, pos[i].rotation) as GameObject;
            mob.Add(go);
        }

        StartCoroutine(checkWin());
    }

    IEnumerator checkWin()
    {
        while(!Win)
        {
            int remain = 0;
            foreach(GameObject t in mob)
            {
                if(t == null)
                {
                    continue;
                }
                if(t.GetComponent<MonsterHP>().HP > 0)
                {
                    remain += 1;
                }
            }
            if(remain == 0)
            {
                Win = true;
                WinGame();
            }
            yield return new WaitForSeconds(0.5f);
        }
    }

    void WinGame()
    {
        PlayerInventory.Instance.ClearHand();
        Camera.main.GetComponent<BlurOptimized>().enabled = true;
        victoryPanel.SetActive(true);
        dotPanel.SetActive(false);
        print("승리");
    }
}
