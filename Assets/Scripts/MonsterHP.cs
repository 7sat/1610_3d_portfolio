﻿using UnityEngine;
using System.Collections;

public class MonsterHP : MonoBehaviour 
{
    public float HP;
    Animator animator;
    MonsterMove monsterMove;
    NavMeshAgent navMeshAgent;

    public bool canStun;

    void Awake()
    {
        animator = GetComponent<Animator>();
        monsterMove = GetComponent<MonsterMove>();
        navMeshAgent = GetComponent<NavMeshAgent>();
    }

	void ProcessDamage(float damage)
    {
        if (monsterMove.state == MonsterMove.STATE.DIE) return;

        HP -= damage;
        if (HP <= 0 && monsterMove.state != MonsterMove.STATE.DIE)
        {
            animator.SetTrigger("Die");
            monsterMove.state = MonsterMove.STATE.DIE;
            navMeshAgent.SetDestination(transform.position);
            GetComponent<Collider>().enabled = false;
            Destroy();
        }
        else
        {
            if (canStun)
            {
                animator.SetTrigger("Hit");
                stun();
            }
        }
    }

    void Destroy()
    {
        Destroy(gameObject, 5f);
    }

    public void stun()
    {
        navMeshAgent.Stop();
        monsterMove.isStun = true;
    }

    public void stunEnd()
    {
        monsterMove.isStun = false;
        navMeshAgent.Resume();
    }
}
