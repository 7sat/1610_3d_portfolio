﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class PlayerInventory : MonoBehaviour
{
    static PlayerInventory instance;
    public static PlayerInventory Instance { get { return instance; } }

    public MouseLock mouseLock;

    public Transform invenPlayerParent;
    public Transform invenUIParent;
    public GameObject invenUI;
    public GameObject uiButton;

    public Transform currentlyHeldItem;
    public Transform[] InvenItemTF = new Transform[18];
    public GameObject[] InvenBtn = new GameObject[18];

    public bool isMoveState;
    int selectedSlotNum = -1;
    public Button UseBtn;
    public Button ThrowBtn;
    public Sprite[] itemIcon = new Sprite[4];

    FirstPersonController fpsCtr;

    bool isMainWeapon = true;

    void Awake()
    {
        instance = this;
        fpsCtr = GetComponent<FirstPersonController>();

        for(int i = 0; i<16; i++)
        {
            GameObject btn = Instantiate(uiButton);
            btn.transform.SetParent(invenUIParent,false);
            int index = i;
            btn.GetComponent<Button>().onClick.AddListener(() => SlotSelect(index));
            InvenBtn[i] = btn;
        }
    }

    void Update()
    {
        if (FinalSpawn.Instance.Win || PlayerHp.Instance.isGameOver) return;

        if (Input.GetKeyDown(KeyCode.I) || Input.GetKeyDown(KeyCode.Escape))
        {
            ToggleInvenUI();
        }

        if(Input.GetKeyDown(KeyCode.Q))
        {
            SwapWeapon();
        }
    }

    public void SlotSelect(int index)
    {
        int oldIndex = selectedSlotNum;

        for(int i = 0; i<18; i++)
        {
            InvenBtn[i].transform.GetChild(0).gameObject.SetActive(false);
        }
        InvenBtn[index].transform.GetChild(0).gameObject.SetActive(true);
        selectedSlotNum = index;

        if (InvenItemTF[index] != null)
        {
            UseBtn.interactable = true;
            ThrowBtn.interactable = true;
        }
        else
        {
            UseBtn.interactable = false;
            ThrowBtn.interactable = false;
        }

        if (isMoveState)
        {
            Transform oldTF = InvenItemTF[oldIndex];
            InvenItemTF[oldIndex] = InvenItemTF[index];
            InvenItemTF[index] = oldTF;
            selectedSlotNum = -1;
            UpdateInvenListAndUI();
        }

        if(InvenItemTF[index] != null || isMoveState)
        {
            isMoveState = !isMoveState;
        }

        if(InvenItemTF[16] != null)
        {
            if(currentlyHeldItem != InvenItemTF[16])
            {
                ActivateInvenItem(16);
                isMainWeapon = true;
            }
        }
        else
        {
            ClearHand();
        }
    }
    public void UseBtnClick()
    {
        Transform oldTF = InvenItemTF[16];
        InvenItemTF[16] = InvenItemTF[selectedSlotNum];
        InvenItemTF[selectedSlotNum] = oldTF;
        selectedSlotNum = 16;
        ActivateInvenItem(selectedSlotNum);

        isMoveState = false;
        ToggleInvenUI();
        isMainWeapon = true;
    }

    public void ClearHand()
    {
        foreach (Transform child in invenPlayerParent)
        {
            if (child.CompareTag("Item"))
            {
                child.gameObject.SetActive(false);
            }
        }
        currentlyHeldItem = null;
        PlayerAmmoBox.Instance.HideAmmoUI();
    }

    public void ThrowBtnClick()
    {
        InvenItemTF[selectedSlotNum].gameObject.SetActive(true);
        InvenItemTF[selectedSlotNum].GetComponent<ItemThrow>().ThrowActions();
        UseBtn.interactable = false;
        ThrowBtn.interactable = false;

        isMoveState = false;
    }

    public void ToggleInvenUI()
    {
        selectedSlotNum = -1;

        isMoveState = false;
        invenUI.SetActive(!invenUI.activeSelf);
        UpdateInvenListAndUI();
        mouseLock.ToggleCursorLock();

        if(Time.timeScale == 0)
        {
            Time.timeScale = 1;
            fpsCtr.enabled = true;
        }
        else
        {
            Time.timeScale = 0;
            fpsCtr.enabled = false;
        }
    }

    public void UpdateInvenListAndUI()
    {
        for (int i = 0; i < 18; i++)
        {
            InvenBtn[i].transform.GetChild(0).gameObject.SetActive(false);
        }
        for (int i = 0; i<18; i++)
        {
            if(selectedSlotNum != -1)
            {
                InvenBtn[selectedSlotNum].transform.GetChild(0).gameObject.SetActive(true);
                if (InvenItemTF[selectedSlotNum] == null)
                {
                    UseBtn.interactable = false;
                    ThrowBtn.interactable = false;
                }
                else
                {
                    UseBtn.interactable = true;
                    ThrowBtn.interactable = true;
                }
            }
            else
            {
                UseBtn.interactable = false;
                ThrowBtn.interactable = false;
            }

            if (InvenItemTF[i] == null)
            {
                InvenBtn[i].GetComponentInChildren<Text>().text = "";
                InvenBtn[i].transform.GetChild(1).gameObject.SetActive(false);
            }
            else
            {
                InvenBtn[i].GetComponentInChildren<Text>().text = InvenItemTF[i].name;

                InvenBtn[i].transform.GetChild(1).gameObject.SetActive(true);
                InvenBtn[i].transform.GetChild(1).GetComponentInChildren<Image>().sprite = itemIcon[InvenItemTF[i].GetComponentInChildren<ItemInfo>().itemCode];
            }
        }
    }

    public void ActivateInvenItem(int inventoryIndex)
    {
        ClearHand();

        foreach (Transform child in invenPlayerParent)
        {
            if (child.CompareTag("Item"))
            {
                currentlyHeldItem = InvenItemTF[inventoryIndex];
                AcitvatedItemSetting();
            }
        }
    }

    void AcitvatedItemSetting()
    {
        currentlyHeldItem.gameObject.SetActive(true);

        if (currentlyHeldItem.GetComponent<ItemInfo>().itemType == ItemInfo.ItemType.Gun)
        {
            PlayerAmmoBox.Instance.gunAmmo = currentlyHeldItem.GetComponent<GunAmmo>();
            GunAccuIndicator.Instance.gunShoot = currentlyHeldItem.GetComponent<GunShoot>();
            PlayerAmmoBox.Instance.UpdateAmmoUI();
            PlayerAmmoBox.Instance.ShowAmmoUI();
            GunAccuIndicator.Instance.ShowIndicator();
            currentlyHeldItem.GetComponent<GunAmmo>().ResetGunReloading();
        }
    }

    void SwapWeapon()
    {
        if(isMainWeapon)
        {
            if(InvenItemTF[17] != null)
            {
                ActivateInvenItem(17);
            }
            else
            {
                ClearHand();
            }
        }
        else
        {
            if (InvenItemTF[16] != null)
            {
                ActivateInvenItem(16);
            }
            else
            {
                ClearHand();
            }
        }
        isMainWeapon = !isMainWeapon;
    }
}


