﻿using UnityEngine;
using System.Collections;

public class GunAmmo : MonoBehaviour 
{
    GunInput gunInput;
    PlayerAmmoBox playerAmmoBox;

    Animator myAnimator;

    public int clipSize;
    public int currentAmmo;
    public string ammoName;

    public float reloadVolume = 0.4f;
    public AudioClip reloadSound;

    void Start()
    {
        gunInput = GetComponent<GunInput>();
        myAnimator = GetComponent<Animator>();
        playerAmmoBox = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerAmmoBox>();
    }

    public void DeductAmmo() // 쏠때마다 실행됨.
    {
        currentAmmo--;
        CheckAmmoStatus();
        playerAmmoBox.UpdateAmmoUI();
    }

    public void CheckAmmoStatus() // 남은 탄약 확인, isGunLoaded 가 참인지 거짓인지 설정.
    {
        if (currentAmmo <= 0)
        {
            currentAmmo = 0;
            gunInput.isGunLoaded = false;
        }
        else if (currentAmmo > 0)
        {
            gunInput.isGunLoaded = true;
        }
    }

    public void TryToReload() // 재장전 애니메이션 실행. 애니메이션 끝부분에서 OnReloadComplete가 실행됨.
    {
        for (int i = 0; i < playerAmmoBox.typesOfAmmunition.Count; i++) // 플레이어가 가지고 있는 각 탄약 종류에 대해.
        {
            if (playerAmmoBox.typesOfAmmunition[i].ammoName == ammoName) // 이 총의 탄약 종류와 일치함.
            {
                // 1) 플레이어가 해당 탄약을 한발이라도 가지고 있어야함. 2) 탄창에 이미 꽉 차있으면 안됨. 3)이미 재장전 중이면 안됨.
                if (playerAmmoBox.typesOfAmmunition[i].ammoCurrentCarried > 0 && currentAmmo != clipSize && !gunInput.isReloading)
                {
                    gunInput.isReloading = true;
                    gunInput.isGunLoaded = false;

                    myAnimator.SetTrigger("Reload");
                    AudioSource.PlayClipAtPoint(reloadSound, transform.position, reloadVolume);
                }
                break;
            }
        }
    }

    public void OnReloadComplete() //재장전. 재장전 애니메이션 끝부분에서 실행됨.
    {
        for (int i = 0; i < playerAmmoBox.typesOfAmmunition.Count; i++) // 플레이어가 가지고 있는 각 탄약 종류에 대해.
        {
            if (playerAmmoBox.typesOfAmmunition[i].ammoName == ammoName) // 이 총의 탄약 종류와 일치함.
            {
                int ammoTopUp = clipSize - currentAmmo;

                if (playerAmmoBox.typesOfAmmunition[i].ammoCurrentCarried >= ammoTopUp) // 완전히 재장전 할 만큼 가지고 있음.
                {
                    currentAmmo += ammoTopUp;
                    playerAmmoBox.typesOfAmmunition[i].ammoCurrentCarried -= ammoTopUp;
                }
                else if (playerAmmoBox.typesOfAmmunition[i].ammoCurrentCarried < ammoTopUp && playerAmmoBox.typesOfAmmunition[i].ammoCurrentCarried != 0) // 일부만 가지고 있음.
                {
                    currentAmmo += playerAmmoBox.typesOfAmmunition[i].ammoCurrentCarried;
                    playerAmmoBox.typesOfAmmunition[i].ammoCurrentCarried = 0;
                }

                break; // 재정전(또는 재장전 시도) 끝남. 반복문 탈출.
            }
        }

        ResetGunReloading();
    }

    public void ResetGunReloading() // 총을 던졌을때도 실행됨.
    {
        gunInput.isReloading = false;
        CheckAmmoStatus();
        playerAmmoBox.UpdateAmmoUI();
    }
}
