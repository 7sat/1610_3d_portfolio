﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;

public class GunShoot : MonoBehaviour
{
    FirstPersonController fpsCtr;
    Transform camTransform;
    Animator myAnimator;

    RaycastHit hit;
    Transform hitTransform;
    Vector3 startPosition = new Vector3(0,0,1);

    public float range;
    public int damage;
    [Tooltip("정확도. 숫자가 작을수록 좋음")]
    public float BaseAccuracy;
    public float AccuracyLowerByShooting; // 작을수록 좋음. 총질하면 커짐.
    public float Recoil;
    public float forceToApply;
    public float shootVolume;
    public AudioClip[] shootSound;
    public ParticleSystem muzzleFlash;
    public GameObject defaultHitEffect;
    public GameObject enemyHitEffect;

    public GameObject lightEffect;

    Transform recoilTransform;
    Transform weaponRecoilTransform;

    public GameObject bulletHoleDecal;

    void Awake()
    {
        fpsCtr = GameObject.FindGameObjectWithTag("Player").GetComponent<FirstPersonController>();
        camTransform = GameObject.FindGameObjectWithTag("MainCamera").transform;
        myAnimator = GetComponent<Animator>();
        recoilTransform = fpsCtr.gameObject.transform.GetChild(0);
        weaponRecoilTransform = Camera.main.transform.GetChild(0);
    }

    public void OpenFire()
    {
        ShotEffect();

        float accuracy = (BaseAccuracy * PlayerState.Instance.playerCurrentSpeed) * 0.6f + BaseAccuracy + AccuracyLowerByShooting;
        if (PlayerState.Instance.isAim) accuracy *= 0.5f;

        //print(accuracy);

        Vector3 shootDir = camTransform.forward + new Vector3(Random.Range(-accuracy, accuracy), Random.Range(-accuracy, accuracy), 0);

        if (Physics.Raycast(camTransform.TransformPoint(startPosition), shootDir, out hit, range)) // (raycast의 12번 구조) 포워드는 0,0,1임.
        {
            Debug.DrawLine(camTransform.TransformPoint(startPosition), hit.point, Color.red, 0.5f);

            hitTransform = hit.transform;

            if (hit.transform.CompareTag("Enemy") || hit.transform.CompareTag("Head"))
            {
                EnemyHitEffect();
            }
            else
            {
                HitEffect();
            }
        }

        RecoilMovement();
    }

    void Update()
    {
        if(AccuracyLowerByShooting > 0)
        {
            if(Input.GetMouseButton(0))
            {
                AccuracyLowerByShooting -= 0.001f;
            }
            else
            {
                AccuracyLowerByShooting -= 0.005f;
            }
        }
        if(AccuracyLowerByShooting > 0.1)
        {
            AccuracyLowerByShooting = 0.1f;
        }
        weaponRecoilTransform.GetComponent<Animator>().SetFloat("Pos",AccuracyLowerByShooting * 100);
    }

    void ShotEffect()
    {
        myAnimator.SetTrigger("Shoot");
        StopAllCoroutines();
        StartCoroutine(lightEffectCor());
        AccuracyLowerByShooting += BaseAccuracy;

        int index = Random.Range(0, shootSound.Length);
        AudioSource.PlayClipAtPoint(shootSound[index], transform.position, shootVolume);
        muzzleFlash.Play();
    }

    void RecoilMovement()
    {
        if(PlayerState.Instance.isAim)
        {
            recoilTransform.Rotate(-Recoil/3, 0, 0);
            fpsCtr.gameObject.transform.Rotate(0, Recoil / 9, 0);
        }
        else
        {
            recoilTransform.Rotate(-Recoil, 0, 0);
            fpsCtr.gameObject.transform.Rotate(0, Recoil / 3, 0);
        }

        fpsCtr.m_MouseLook.Init(fpsCtr.gameObject.transform, Camera.main.transform);

        fpsCtr.m_MouseLook.MinimumX += Recoil;
        fpsCtr.m_MouseLook.MaximumX += Recoil;

        if (recoilTransform.eulerAngles.x <= 360 && recoilTransform.eulerAngles.x != 0)
        {
            fpsCtr.enabled = false;

            Quaternion camRot = Camera.main.transform.rotation;

            recoilTransform.localRotation = Quaternion.identity;
            fpsCtr.m_MouseLook.MinimumX = -90;
            fpsCtr.m_MouseLook.MaximumX = 90;

            Camera.main.transform.rotation = camRot;
            fpsCtr.m_MouseLook.Init(fpsCtr.gameObject.transform, Camera.main.transform);

            fpsCtr.enabled = true;
        }
    }

    void HitEffect()
    {
        Instantiate(defaultHitEffect, hit.point, Quaternion.identity);

        GameObject decal = (GameObject)Instantiate(bulletHoleDecal, hit.point, Quaternion.LookRotation(hit.normal * -1));
        decal.transform.SetParent(hit.transform);

        if (hitTransform.GetComponent<Rigidbody>() != null)
        {
            hitTransform.GetComponent<Rigidbody>().AddForce(transform.forward * forceToApply, ForceMode.Impulse);
        }
    }

    void EnemyHitEffect()
    {
        Instantiate(enemyHitEffect, hit.point, Quaternion.identity);
        if(hitTransform.tag == "Head")
        {
            hitTransform.root.SendMessage("ProcessDamage", damage*3, SendMessageOptions.DontRequireReceiver);
        }
        else
        {
            hitTransform.root.SendMessage("ProcessDamage", damage, SendMessageOptions.DontRequireReceiver);
        }
    }

    IEnumerator lightEffectCor()
    {
        lightEffect.SetActive(true);
        yield return new WaitForSeconds(0.05f);
        lightEffect.SetActive(false);
    }
}
